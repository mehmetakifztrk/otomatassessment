package com.odeal.otomat;

import com.odeal.otomat.entity.Inventory;
import com.odeal.otomat.mapper.RequestDto;
import com.odeal.otomat.repository.BillRepository;
import com.odeal.otomat.repository.InventoryRepository;
import com.odeal.otomat.service.impl.AutomatServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AutomatServiceImplTest {

    @InjectMocks
    AutomatServiceImpl automatService;

    @Mock
    InventoryRepository inventoryRepository;

    @Mock
    BillRepository billRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldGetAllInventory() {

        //Given
        List<Inventory> inventories = Collections.singletonList(Inventory.builder().name("Topkek")
                .amount(1L).id(1L).quantity(1L).amount(1L).isHot(false).build());
        when(inventoryRepository.findAll()).thenReturn(inventories);
        //When
        List<Inventory> allInventory = automatService.getAllInventory();

        //Then
        verify(inventoryRepository).findAll();
    }
    @Test
    void shouldSales() {

        //Given
        Inventory topKek = Inventory.builder().name("Topkek").amount(1L).id(1L).quantity(5L).amount(1L)
                .isHot(false).build();
        RequestDto requestDto = RequestDto.builder().sugar(false).itemId(1L).paid(4L).paymentMethod("Temassız")
                .paymentType("Kart").quantity(1L).build();
        when(inventoryRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(topKek));
        //When
        automatService.sales(requestDto);

        //Then
        verify(inventoryRepository).findById(anyLong());
    }
}

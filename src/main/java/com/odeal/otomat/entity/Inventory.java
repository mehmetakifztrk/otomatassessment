package com.odeal.otomat.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "INVENTORY")
public class Inventory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "INVENTORY_SEQ")
    @SequenceGenerator(name = "INVENTORY_SEQ", sequenceName = "INVENTORY_SEQ", initialValue = 100)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "IS_HOT")
    private boolean isHot;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "AMOUNT")
    private Long amount;

}

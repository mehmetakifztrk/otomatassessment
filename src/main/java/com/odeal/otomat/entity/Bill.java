package com.odeal.otomat.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "BILL")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "BILL_SEQ")
    @SequenceGenerator(name = "BILL_SEQ", sequenceName = "BILL_SEQ", initialValue = 100)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "PAYMENT_TYPE")
    private String paymentType;

    @Column(name = "PAYMENT_METHOD")
    private String paymentMethod;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "TOTAL_AMOUNT")
    private Long totalAmount;

    @Column(name = "CHANGE")
    private Long change;

}

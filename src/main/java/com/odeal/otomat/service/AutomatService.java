package com.odeal.otomat.service;

import com.odeal.otomat.entity.Inventory;
import com.odeal.otomat.mapper.RequestDto;
import com.odeal.otomat.mapper.ResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AutomatService {
    List<Inventory> getAllInventory();
    ResponseDto sales(RequestDto requestDto);
}

package com.odeal.otomat.service.impl;

import com.odeal.otomat.entity.Bill;
import com.odeal.otomat.entity.Inventory;
import com.odeal.otomat.mapper.RequestDto;
import com.odeal.otomat.mapper.ResponseDto;
import com.odeal.otomat.repository.BillRepository;
import com.odeal.otomat.repository.InventoryRepository;
import com.odeal.otomat.service.AutomatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class AutomatServiceImpl implements AutomatService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    BillRepository billRepository;


    @Override
    public List<Inventory> getAllInventory() {
        return inventoryRepository.findAll();
    }

    @Override
    public ResponseDto sales(RequestDto requestDto) {
        ResponseDto responseDto = null;
        try{
            Bill bill = null;
            long change = 0;
            Inventory byId = inventoryRepository.findById(requestDto.getItemId()).orElse(new Inventory());
            long sum = byId.getAmount() * requestDto.getQuantity();
            if (isValid(requestDto, byId, sum)){
                change = requestDto.getPaymentType().equals("Nakit") ? requestDto.getPaid() - sum : 0;
                bill = Bill.builder().itemId(byId.getId())
                        .quantity(requestDto.getQuantity()).paymentType(requestDto.getPaymentType())
                        .paymentMethod(requestDto.getPaymentMethod()).totalAmount(sum).change(change).build();
                billRepository.save(bill);
                responseDto = ResponseDto.builder().change(change).itemId(byId.getId())
                        .itemName(byId.getName()).paid(requestDto.getPaid()).paymentMethod(requestDto.getPaymentMethod())
                        .paymentType(requestDto.getPaymentType()).totalAmount(sum).quantity(requestDto.getQuantity()).build();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return responseDto;
    }

    private boolean isValid(RequestDto requestDto, Inventory inventory, long sum){
        if (requestDto.getPaid()>sum && requestDto.getQuantity()<inventory.getQuantity()
                && requestDto.getPaid()>0 && (inventory.isHot() || (!inventory.isHot() && !requestDto.isSugar()))){
            return true;
        }else {
            return false;
        }
    }

}

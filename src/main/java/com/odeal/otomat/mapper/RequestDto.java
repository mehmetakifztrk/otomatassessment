package com.odeal.otomat.mapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestDto {
    private Long itemId;
    private Long quantity;
    private String paymentType;
    private String paymentMethod;
    private Long paid;
    private boolean sugar;
}

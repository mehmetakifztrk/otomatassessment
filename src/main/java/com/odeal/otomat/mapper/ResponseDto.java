package com.odeal.otomat.mapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDto {
    private Long itemId;
    private String itemName;
    private Long quantity;
    private String paymentType;
    private String paymentMethod;
    private Long totalAmount;
    private Long paid;
    private Long change;
}

package com.odeal.otomat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OtomatApplication {

	public static void main(String[] args) {
		SpringApplication.run(OtomatApplication.class, args);
	}

}

package com.odeal.otomat.controller;

import com.odeal.otomat.entity.Inventory;
import com.odeal.otomat.mapper.RequestDto;
import com.odeal.otomat.mapper.ResponseDto;
import com.odeal.otomat.service.AutomatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api")
@RestController
@Slf4j
public class AutomatCtrl {

    @Autowired
    AutomatService automatService;

    @GetMapping("getAllInventory")
    public ResponseEntity<List<Inventory>> getAllInventory() {
        List<Inventory> allInventory = automatService.getAllInventory();
        if (allInventory == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(allInventory);
    }

    @PostMapping("sales")
    public ResponseEntity<ResponseDto> sales(@RequestBody RequestDto requestDto) {
        ResponseDto sales = automatService.sales(requestDto);
        if (sales == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        return ResponseEntity.status(HttpStatus.OK).body(sales);
    }


}
